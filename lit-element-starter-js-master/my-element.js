import {LitElement, html, css} from 'lit-element';

export class MyElement extends LitElement {
    static get styles() {
        return css`
        .show-alert{
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
            border: 1px solid black;
            width:500px;
            height: 100px;
            top: calc(50% - 50px);
            color: black;
            position: fixed;
            left: calc(50% - 250px);
            opacity: 1;
            border-radius: 10px;
            transition: opacity 0.2s ease-out;
        }
        
        .no-alert{
            display:none;
            opacity:0;
            transition: all 0.3s ease-out;
        }
        
        .success{
            background-color: #4CAF50;
        }
        .error{
            background-color: #f44336;
        }
        .warning{
            background-color: #ff9800;
        }
        p{
            font-size: x-large;
            width:100%;
            display: flex;
            justify-content: center;
            align-items: center;
            border-bottom: 2px solid black;
            margin: 0;
            background-color: red;
            border-radius: 10px 10px 0px 0px;

        }
      
    `;
    }

    static get properties() {
        return {
            show: {type: Boolean},
            alertType: {type: String},
            timeout: {type: Number},
            title:{type: String},

        };
    }

    constructor() {
        super();
        this.show = false;
        this.alertType = 'success';
        this.timeout=2000;
        this.title='';
    }

    render() {
        return html`
      <div class = "${this.show ? 'show-alert '+ this.alertType : 'no-alert'}">
        ${this.title !== '' ? 
            html `<p>${this.title}</p>`: null}
         <slot name="text"></slot>
      </div>
    `;
    }

    showAlert() {
        this.show = true;
        setTimeout(()=>{
            this.show=false
        }, this.timeout);
    }

}

window.customElements.define('my-element', MyElement);
