# Framework-Design Assessment
This is a web component creating a custom alert box,  based on LitElement.

## Setup
Steps in order to run this web component locally: 
1. Open terminal and clone project:
```
 git clone git@gitlab.com:MadalinaBoca/Framework-Design.git

``` 
2. Run:
```
npm install
```
3. To start the project, run:
```
npm run serve
```
4. In browser, visit the address:  
```
http://localhost:8000/dev
```
_Please be aware, the port can be different on your machine._

## Usage
Features to be tested:
1. Custom background color for types of alerts: success, warning, error.
2. The Alert will close automatically after given ms. By default 2 s. 
3. The Alert supports custom text inside a < p > tag.
4. The Alert supports custom title.
```
<my-element id="alert1" timeout="5000" title="This is my custom title !" alertType="error">
    <p slot="text">This is my custom error text inside the alert.</p>
</my-element>
```

## Properties

- **show**: boolean property used in order to change the alert box state, by default: false (= closed), when a button is clicked the property is true (= open).
- **alertType**: text property, can be either: success | warning | error.
- **timeout**: number property used to specify in ms the closing time for Alert. By default 2000ms, if no timeout property is given.
- **title**: text property to be inserted in the title section of the Alert. If no title is given, then the body section will occupy the whole space.
- **< p slot ="text">...< /p>**: text to be inserted in the body section of the Alert.

## Methods
- **showAlert**: use it to open the Alert box.
```
 document.getElementById("buttonSuccess")
        .addEventListener("click",() => { document.getElementById("alert1").showAlert();});
```

## Style customization
Based on alert type, the background color of the alert changes:

Alert Type | Color
---------- | ------
success    | background-color: #4CAF50 (green)
warning    | background-color: #ff9800 (yellow)
error      | background-color: #f44336 (red)
